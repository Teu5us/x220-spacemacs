#!/bin/sh
#
thisconf="$(find $HOME/ -name x220-spacemacs 2>/dev/null)"
confrepo="$HOME/.emacs.d"
[ -d "$confrepo" ] && echo ".emacs.d exists!" && exit 0
[ -f "$HOME/.spacemacs" ] && echo "dotspacemacs exists!" && exit 0
git clone https://github.com/syl20bnr/spacemacs "$confrepo"
case "$(uname)" in
    Linux) ln -s "$thisconf"/.spacemacs "$HOME/.spacemacs" ;;
    Darwin) ln -s "$thisconf"/.spacemacs.mac "$HOME/.spacemacs" ;;
esac
