;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused
   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t
   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(
     yaml
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     helm
     (auto-completion :variables
                      auto-completion-enable-snippets-in-popup t)
     ;; better-defaults
     emacs-lisp
     git
     markdown
     org
     (shell :variables
            shell-default-height 30
            shell-default-position 'bottom
            shell-enable-smart-eshell t
            shell-default-shell 'eshell
            shell-default-term-shell "/usr/bin/zsh"
            shell-default-full-span t)
     spell-checking
     syntax-checking
     ;; version-control
     pandoc
     latex
     (ranger :variables
             ranger-cleanup-on-disable t)
     exwm
     themes-megapack
     colors
     search-engine
     html
     shell-scripts
     javascript
     gtags
     (c-c++ :variables
            c-c++-enable-clang-support t)
     semantic
     python
     )
   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages '(google-translate
                                      reverse-im
                                      ob-translate
                                      visual-fill
                                      visual-fill-column
                                      writeroom-mode
                                      darkroom
                                      exwm-edit
                                      auto-dictionary
                                      latex-preview-pane
                                      yasnippet-snippets
                                      auto-complete-auctex
                                      pdf-tools
                                      org-edit-latex
                                      )
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '(google-translate)
   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '()
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and uninstall any
   ;; unused packages as well as their unused dependencies.
   ;; `used-but-keep-unused' installs only the used packages but won't uninstall
   ;; them if they become unused. `all' installs *all* packages supported by
   ;; Spacemacs and never uninstall them. (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 5
   ;; If non nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil
   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'.
   dotspacemacs-elpa-subdirectory nil
   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim
   ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   ;; dotspacemacs-startup-banner 'official
   dotspacemacs-startup-banner nil
   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'."
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))
   ;; True if the home buffer should respond to resize events.
   dotspacemacs-startup-buffer-responsive t
   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'org-mode
   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(
                         solarized-dark
                         solarized-light
                         ;; omtose-darker
                         ;; moe-dark
                         ;; noctilux
                         ;; professional
                         )
   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   ;; dotspacemacs-default-font '("Source Code Pro"
   ;;                             :size 16
   ;;                             :weight normal
   ;;                             :width normal
   ;;                             :powerline-scale 0.2
   ;;                             :powerline-offset 0.5)
   ;; dotspacemacs-default-font '("Droid Sans Mono Dotted for Powerline"
   ;;                             :size 14
   ;;                             :weight normal
   ;;                             :width normal
   ;;                             ;; :powerline-scale 1.1
   ;;                             :powerline-scale 1.1)
   ;;                             ;; :powerline-offset 0.5)
   dotspacemacs-default-font '("FantasqueSansMono"
                               :size 16
                               :weight normal
                               :width normal
                               :powerline-scale 1.2)
   ;; :powerline-offset 0.5)
   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The key used for Emacs commands (M-x) (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"
   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"
   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs C-i, TAB and C-m, RET.
   ;; Setting it to a non-nil value, allows for separate commands under <C-i>
   ;; and TAB or <C-m> and RET.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil
   ;; If non nil `Y' is remapped to `y$' in Evil states. (default nil)
   dotspacemacs-remap-Y-to-y$ t
   ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
   ;; there. (default t)
   dotspacemacs-retain-visual-state-on-shift t
   ;; If non-nil, J and K move lines up and down when in visual mode.
   ;; (default nil)
   dotspacemacs-visual-line-move-text nil
   ;; If non nil, inverse the meaning of `g' in `:substitute' Evil ex-command.
   ;; (default nil)
   dotspacemacs-ex-substitute-global nil
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil
   ;; If non nil then the last auto saved layouts are resume automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil
   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1
   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'original
   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize t
   ;; if non nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header t
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; Controls fuzzy matching in helm. If set to `always', force fuzzy matching
   ;; in all non-asynchronous sources. If set to `source', preserve individual
   ;; source settings. Else, disable fuzzy matching in all sources.
   ;; (default 'always)
   dotspacemacs-helm-use-fuzzy 'always
   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   dotspacemacs-enable-paste-transient-state t
   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4
   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom
   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar nil
   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil
   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 80
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90
   ;; If non nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t
   ;; If non nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t
   ;; If non nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols t
   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling nil
   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil
   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil
   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil
   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
   ;; (default '("ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now. (default nil)
   dotspacemacs-default-package-repository nil
   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed'to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'trailing
   ))

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init', before layer configuration
executes.
 This function is mostly useful for variables that need to be set
before packages are loaded. If you are unsure, you should try in setting them in
`dotspacemacs/user-config' first."
  ;; + COMPILER
  (defun compiler ()
    "Compile the document."
    (interactive)
    (shell-command
     (format "/home/$USER/.scripts/tools/compiler %s > /dev/null 2>&1"
             (shell-quote-argument (buffer-file-name))))
    (revert-buffer t t t))
  (evil-leader/set-key "cz" 'compiler)
  ;; - COMPILER
  ;; RUSSIAN BEG/END LINE
  (evil-leader/set-key "H" 'evil-first-non-blank)
  (evil-leader/set-key "L" 'evil-end-of-line)
  ;; EVIL-ORG
  ;; (add-hook 'org-mode-hook 'evil-org-mode)
  ;; ;; (evil-org-set-key-theme '(textobjects insert navigation additional shift todo heading))
  ;; (evil-org-set-key-theme '(textobjects insert navigation additional shift))
  ;; (setq org-special-ctrl-a/e t)
  ;; MY RANGER DIRS
  (defun my/ranger-go (path)
    "Go subroutine"
    (interactive
     (list
      (read-char-choice
       "e   : /etc
       u   : /usr
       d   : ~/Documents
       D   : ~/Downloads
       b   : ~/Nextcloud
       l   : follow directory link
       L   : follow selected file
       o   : /opt
       v   : /var
       h   : ~/
       m   : /media
       M   : /mnt
       s   : /srv
       r : ~/gits/gitlab
       / : /
       R   : ranger . el location
       > "
       '(?q ?e ?u ?d ?D ?b ?l ?L ?o ?v ?m ?M ?s ?r ?R ?/ ?h ?g ?D ?j ?k ?T ?t ?n ?c))))
    (message nil)
    (let* ((c (char-to-string path))
           (new-path
            (cl-case (intern c)
              ('e "/etc")
              ('u "/usr")
              ('d "~/Documents")
              ('D "~/Downloads")
              ('b "~/Nextcloud")
              ('l (file-truename default-directory))
              ('L (file-truename (dired-get-filename)))
              ('o "/opt")
              ('v "/var")
              ('m "/media")
              ('M "/mnt")
              ('s "/srv")
              ('r "~/gits/gitlab")
              ('R (file-truename (file-name-directory (find-library-name "ranger.el"))))
              ('h  "~/")
              ('/ "/")))
           (alt-option
            (cl-case (intern c)
              ;; Subdir Handlng
              ('j 'ranger-next-subdir)
              ('k 'ranger-prev-subdir)
              ;; Tab Handling
              ('n 'ranger-new-tab)
              ('T 'ranger-prev-tab)
              ('t 'ranger-next-tab)
              ('c 'ranger-close-tab)
              ('g 'ranger-goto-top))))
      (when (string-equal c "q")
        (keyboard-quit))
      (when (and new-path (file-directory-p new-path))
        (ranger-find-file new-path))
      (when (eq system-type 'windows-nt)
        (when (string-equal c "D")
          (ranger-show-drives)))
      (when alt-option
        (call-interactively alt-option))))
  (advice-add 'ranger-go :override #'my/ranger-go)
  (defun brightup ()
    "Increase brightness by 10%."
    (interactive)
    (shell-command
     (format "xbacklight -inc 10")))
  ;; (global-set-key (kbd "<XF86MonBrightnessUp>") 'brightup)

  (defun brightdown ()
    "Decrease brightness by 10%."
    (interactive)
    (shell-command
     (format "xbacklight -dec 10")))
  ;; (global-set-key (kbd "<XF86MonBrightnessDown>") 'brightdown)

  (defun voldown ()
    "Decrease volume by 10%."
    (interactive)
    (shell-command
     (format "pulsemixer --id 0 --change-volume -5")))
  ;; (global-set-key (kbd "<XF86AudioLowerVolume>") 'voldown)

  (defun volup ()
    "Increase volume by 5%."
    (interactive)
    (shell-command
     (format "pulsemixer --id 0 --change-volume +5")))
  ;; (global-set-key (kbd "<XF86AudioRaiseVolume>") 'volup)

  (defun volmid ()
    "Set volume to 50%."
    (interactive)
    (shell-command
     (format "pulsemixer --id 0 --set-volume 50")))
  ;; (global-set-key (kbd "<XF86AudioRaiseVolume>") 'volmid)

  (defun dmenumount ()
    "Mount drives."
    (interactive)
    (shell-command
     (format "dmenumount")))

  (defun dmenuumount ()
    "Unmount drives."
    (interactive)
    (shell-command
     (format "dmenuumount")))

  (defun dmenususpend ()
    "Suspend."
    (interactive)
    (async-shell-command
     (format "prompt 'Suspend?' 'systemctl suspend'")))

  (defun myxrdb ()
    "Load Xresources."
    (interactive)
    (async-shell-command
     (format "xrdb ~/.Xresouces")))

(fset 'run-goldendict-on-selection
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([121 8388722 103 111 108 100 101 110 100 105 99 116 32 34 25 34 return] 0 "%d")) arg)))

(fset 'gdict-search
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([8388722 103 111 108 100 101 110 100 105 99 116 32 34 36 40 120 99 108 105 112 32 45 111 32 45 115 101 108 101 99 116 105 111 110 32 112 114 105 109 97 114 121 41 34 return] 0 "%d")) arg)))

(fset 'surfbrowser
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([8388722 115 117 114 102 98 tab return s-escape] 0 "%d")) arg)))

(fset 'st-ranger
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([8388722 115 116 32 45 101 32 114 97 110 103 101 114 return s-escape] 0 "%d")) arg)))

(fset 'st-htop
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([8388722 115 116 32 45 101 32 104 116 111 112 return s-escape] 0 "%d")) arg)))

(fset 'st
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([8388722 115 116 return s-escape] 0 "%d")) arg)))

(fset 'firefox
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([8388722 102 105 114 101 102 111 120 return s-escape] 0 "%d")) arg)))

(fset 'clipmenu
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([8388722 99 108 105 112 109 101 110 117 return] 0 "%d")) arg)))

  ;; AUTO-DICTIONARY
  (use-package auto-dictionary)
  (add-hook 'flyspell-mode-hook (lambda () (auto-dictionary-mode 1)))
  (flyspell-mode t)
  (add-hook 'tex-mode-hook (function (lambda () (setq ispell-parser 'tex))))
  (add-hook 'latex-mode-hook (function (lambda () (setq ispell-parser 'tex))))
  )

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,
you should place your code here."
  (setq-default fill-column 81)
  (global-visual-line-mode t)

  ;; GOOGLE TRANSLATE
  (use-package google-translate
    :ensure t
    :init
    (when (and (string-match "0.11.17"
                             (google-translate-version))
               (>= (time-to-seconds)
                   (time-to-seconds
                    (encode-time 0 0 0 23 9 2018))))
      (defun google-translate--get-b-d1 ()
        ;; TKK='430675.2721866130'
        (list 430675 2721866130))))
  (require 'google-translate-smooth-ui)
  (setq google-translate-translation-directions-alist
        '(("en" . "ru") ("ru" . "en") ("de" . "en") ("en" . "de") ("de" . "ru") ("ru" . "de")))
  ;; (setq google-translate-show-phonetic t)
  (require 'ob-translate)

  ;; RUSSIAN SUPPORT
  (setq default-input-method "russian-computer")
  (use-package reverse-im
    :demand t
    :config
    (reverse-im-activate "russian-computer"))

  ;; MARKDOWN
  (use-package markdown-mode
    :ensure t
    :commands (markdown-mode gfm-mode)
    :mode (("README\\.md\\'" . gfm-mode)
           ("\\.md\\'" . markdown-mode)
           ("\\.rmd\\'" . markdown-mode)
           ("\\.markdown\\'" . markdown-mode))
    :init (setq markdown-command "multimarkdown"))

  ;; SCROLL
  (setq redisplay-dont-pause t
        scroll-margin 10
        scroll-conservatively 10000
        scroll-preserve-screen-position 1)
  (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
  (setq mouse-wheel-progressive-speed nil) ;; don’t accelerate scrolling
  (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  (setq scroll-step 1) ;; keyboard scroll one line at a time
  (setq auto-window-vscroll nil)
  ;; SPELLING
  (setq ispell-program-name "aspell")          ; Use aspell to correct mistakes
  ;; ORG
  (global-set-key "\C-cl" 'org-store-link)
  (global-set-key "\C-ca" 'org-agenda)
  (global-set-key "\C-cc" 'org-capture)
  (global-set-key "\C-cb" 'org-switchb)
  (add-hook 'org-mode-hook 'turn-on-font-lock)
  (setq org-startup-align-all-table t)
  ;; LATEX EXPORT FOR ORG
  (setq org-latex-pdf-process
        '("xelatex -interaction nonstopmode %f"
          "xelatex -interaction nonstopmode %f")) ;; for multiple passes
  ;; MULTIPLE CURSORS
  (global-evil-mc-mode 1)
  ;; + AUTO-INSTALL
  ;; (add-to-list 'load-path (expand-file-name "~/elisp"))
  ;; (require 'auto-install)
  ;; (setq auto-install-directory "~/.emacs.d/auto-install/")
  ;; - AUTO-INSTALL
  ;; PERFECT MARGIN
  ;; (require 'perfect-margin)
  ;; (evil-leader/set-key "Tw" 'perfect-margin-mode)
  ;; (add-hook 'text-mode-hook 'perfect-margin-mode)
  ;; (add-hook 'markdown-mode-hook 'perfect-margin-mode)
  ;; (add-hook 'org-mode-hook 'perfect-margin-mode)
  ;; (add-hook 'latex-mode-hook 'perfect-margin-mode)
  ;; (add-hook 'tex-mode-hook 'perfect-margin-mode)
  ;; VISUAL FILL COLUMN
  (add-hook 'text-mode-hook 'turn-on-visual-line-mode)
  (add-hook 'markdown-mode-hook 'turn-on-visual-line-mode)
  (add-hook 'org-mode-hook 'turn-on-visual-line-mode)
  (add-hook 'latex-mode-hook 'turn-on-visual-line-mode)
  (add-hook 'tex-mode-hook 'turn-on-visual-line-mode)
  (add-hook 'text-mode-hook 'visual-fill-column-mode)
  (add-hook 'markdown-mode-hook 'visual-fill-column-mode)
  (add-hook 'org-mode-hook 'visual-fill-column-mode)
  (add-hook 'latex-mode-hook 'visual-fill-column-mode)
  (add-hook 'tex-mode-hook 'visual-fill-column-mode)
  ;; WRITEROOM
  (use-package writeroom-mode
    :commands (writeroom-mode)
    :init
    (evil-leader/set-key "D" 'writeroom-mode)
    :config
    (setq writeroom-restore-window-config t))
  ;; DARKROOM
  (spacemacs|add-toggle darkroom-mode
    :status darkroom-mode
    :on (darkroom-mode)
    :off (darkroom-mode -1)
    :documentation "Turn on darkroom mode."
    :evil-leader "d")
  ;; LEAVE ONE ACTIVE WINDOW
  (evil-leader/set-key "w1" 'spacemacs/toggle-maximize-buffer)
  ;; EXWM-EDIT
  (use-package exwm-edit)
  ;; MY EXWM KEYS
  (exwm-input-set-key (kbd "s-<f4>") 'dmenususpend)
  (exwm-input-set-key (kbd "s-<f9>") 'dmenumount)
  (exwm-input-set-key (kbd "s-<f10>") 'dmenuumount)
  (exwm-input-set-key (kbd "s-}") 'brightup)
  (exwm-input-set-key (kbd "s-Ъ") 'brightup)
  (exwm-input-set-key (kbd "s-{") 'brightdown)
  (exwm-input-set-key (kbd "s-Х") 'brightdown)
  (exwm-input-set-key (kbd "s-+") 'volmid)
  (exwm-input-set-key (kbd "s-=") 'volup)
  (exwm-input-set-key (kbd "s--") 'voldown)
  (exwm-input-set-key (kbd "s-<return>") 'st)
  (exwm-input-set-key (kbd "s-x") 'gdict-search)
  (exwm-input-set-key (kbd "s-ч") 'gdict-search)
  (exwm-input-set-key (kbd "s-c") 'clipmenu)
  (exwm-input-set-key (kbd "s-с") 'clipmenu)
  (exwm-input-set-key (kbd "s-Q") 'spacemacs/delete-window)
  (exwm-input-set-key (kbd "s-Й") 'spacemacs/delete-window)
  (evil-leader/set-key "Gt" 'run-goldendict-on-selection)
  (evil-leader/set-key "ar" 'st-ranger)
  (evil-leader/set-key "ah" 'st-htop)
  (evil-leader/set-key "aw" 'surfbrowser)
  (evil-leader/set-key "aW" 'firefox)
  ;; STANDARD EXWM SUPER-KEYS FOR RUSSIAN LAYOUT
  (exwm-input-set-key (kbd "s-й") 'spacemacs/kill-this-buffer)
  (exwm-input-set-key (kbd "s-ц") 'exwm-workspace-switch)
  (exwm-input-set-key (kbd "s-а") #'spacemacs/exwm-layout-toggle-fullscreen)
  (exwm-input-set-key (kbd "s-к") #'spacemacs/exwm-app-launcher)
  (exwm-input-set-key (kbd "s-г") #'winner-undo)
  (exwm-input-set-key (kbd "s-Г") #'winner-redo)
  ;; (exwm-input-set-key (kbd "s-и") #'ivy-switch-buffer)
  (exwm-input-set-key (kbd "s-и") #'helm-buffers-list)
  (exwm-input-set-key (kbd "s-р") #'evil-window-left)
  (exwm-input-set-key (kbd "s-о") #'evil-window-down)
  (exwm-input-set-key (kbd "s-л") #'evil-window-up)
  (exwm-input-set-key (kbd "s-д") #'evil-window-right)
  (exwm-input-set-key (kbd "s-Р") #'evil-window-move-far-left)
  (exwm-input-set-key (kbd "s-О") #'evil-window-move-very-bottom)
  (exwm-input-set-key (kbd "s-Л") #'evil-window-move-very-top)
  (exwm-input-set-key (kbd "s-Д") #'evil-window-move-far-right)
  (exwm-input-set-key (kbd "M-s-р") #'spacemacs/shrink-window-horizontally)
  (exwm-input-set-key (kbd "M-s-о") #'spacemacs/enlarge-window)
  (exwm-input-set-key (kbd "M-s-л") #'spacemacs/shrink-window)
  (exwm-input-set-key (kbd "M-s-д") #'spacemacs/enlarge-window-horizontally)
  (exwm-input-set-key (kbd "s-ь") #'spacemacs/toggle-maximize-buffer)
  (exwm-input-set-key (kbd "s-ъ") #'spacemacs/exwm-workspace-next)
  (exwm-input-set-key (kbd "s-х") #'spacemacs/exwm-workspace-prev)
  ;; EXWM RUN SPLIT/VSPLIT
  (exwm-input-set-key (kbd "s-s") #'spacemacs/exwm-launch-split-below)
  (exwm-input-set-key (kbd "s-ы") #'spacemacs/exwm-launch-split-below)
  (exwm-input-set-key (kbd "s-v") #'spacemacs/exwm-launch-split-right)
  (exwm-input-set-key (kbd "s-м") #'spacemacs/exwm-launch-split-right)
  ;; ACTIVATE EXWM SHELL BINDING: change C to s
  (exwm-input-set-key (kbd "s-'") #'spacemacs/default-pop-shell)
  (exwm-input-set-key (kbd "s-э") #'spacemacs/default-pop-shell)
  ;; SEARCH-ENGINE
  (setq browse-url-browser-function 'browse-url-generic
        engine/browser-function 'browse-url-generic
        browse-url-generic-program "vimb")
  ;; REPLACE SELECTION WITH CLIPBOARD CONTENTS
  (delete-selection-mode)
  ;; BATTERY IN MODELINE
  (spacemacs/toggle-mode-line-battery-on)
  ;; CHANGE LAYOUT
  ;; (global-set-key (kbd "C-H") 'toggle-input-method)
  ;; LATEX-PREVIEW
  (latex-preview-pane-enable)
  (require 'auto-complete-auctex)
  (myxrdb)
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Linum-format "%7i ")
 '(adaptive-fill-mode nil)
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-term-color-vector
   [unspecified "#14191f" "#d15120" "#81af34" "#deae3e" "#7e9fc9" "#a878b5" "#7e9fc9" "#dcdddd"] t)
 '(background-color "#202020")
 '(background-mode dark)
 '(beacon-color "#eab4484b8035")
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(cursor-color "#cccccc")
 '(cursor-type (quote bar))
 '(custom-safe-themes
   (quote
    ("d600c677f1777c1e4bfb066529b5b73c0179d0499dd4ffa3f599a0fb0cfbd501" "a77ced882e25028e994d168a612c763a4feb8c4ab67c5ff48688654d0264370c" default)))
 '(darkroom-margins (quote (19 . 19)))
 '(display-battery-mode nil)
 '(display-line-numbers (quote relative))
 '(display-time-mode t)
 '(doc-view-continuous t)
 '(doc-view-image-width 500)
 '(doc-view-odf->pdf-converter-function (quote doc-view-odf->pdf-converter-soffice))
 '(doc-view-pdf->png-converter-function (quote doc-view-pdf->png-converter-mupdf))
 '(doc-view-resolution 300)
 '(evil-emacs-state-cursor (quote ("#E57373" hbar)) t)
 '(evil-indent-convert-tabs nil)
 '(evil-insert-state-cursor (quote ("#E57373" bar)) t)
 '(evil-normal-state-cursor (quote ("#FFEE58" box)) t)
 '(evil-visual-state-cursor (quote ("#C5E1A5" box)) t)
 '(evil-want-Y-yank-to-eol t)
 '(exwm-layout-show-all-buffers t)
 '(exwm-workspace-show-all-buffers t)
 '(fci-rule-character-color "#192028")
 '(fci-rule-color "#37474f" t)
 '(fill-column 81)
 '(foreground-color "#cccccc")
 '(fringe-mode 4 nil (fringe))
 '(highlight-indent-guides-auto-enabled nil)
 '(highlight-symbol-colors
   (quote
    ("#FFEE58" "#C5E1A5" "#80DEEA" "#64B5F6" "#E1BEE7" "#FFCC80")))
 '(highlight-symbol-foreground-color "#E0E0E0")
 '(highlight-tail-colors (quote (("#eab4484b8035" . 0) ("#424242" . 100))))
 '(hl-paren-background-colors (quote ("#2492db" "#95a5a6" nil)))
 '(hl-paren-colors (quote ("#ecf0f1" "#ecf0f1" "#c0392b")))
 '(hl-sexp-background-color "#1c1f26")
 '(linum-format " %7i ")
 '(main-line-color1 "#1E1E1E")
 '(main-line-color2 "#111111")
 '(main-line-separator-style (quote chamfer))
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(org-agenda-files (quote ("~/Documents/notes.org" "~/Documents/agenda.org")))
 '(org-babel-load-languages
   (quote
    ((python . t)
     (shell . t)
     (emacs-lisp . t)
     (latex . t))))
 '(org-export-backends (quote (ascii beamer html latex md odt)))
 '(org-export-in-background nil)
 '(org-footnote-auto-adjust t)
 '(org-modules
   (quote
    (org-bbdb org-bibtex org-docview org-eww org-gnus org-info org-irc org-mhe org-rmail org-tempo org-w3m org-toc)))
 '(org-odt-convert-process "LibreOffice")
 '(org-odt-preferred-output-format "docx")
 '(org-odt-styles-file "/home/paul/Documents/LaTeX/orgodt.odt")
 '(org-src-block-faces (quote (("emacs-lisp" (:background "#F0FFF0")))))
 '(org-structure-template-alist
   (quote
    (("la" . "src latex")
     ("eo" . "src translate :dest eo")
     ("deen" . "src translate :src de :dest en")
     ("deru" . "src translate :src de :dest ru")
     ("enru" . "src translate :src en :dest ru")
     ("a" . "export ascii")
     ("c" . "center")
     ("C" . "comment")
     ("e" . "example")
     ("E" . "export")
     ("h" . "export html")
     ("l" . "export latex")
     ("q" . "quote")
     ("s" . "src")
     ("v" . "verse"))))
 '(package-selected-packages
   (quote
    (org-edit-latex yapfify pyvenv pytest pyenv-mode py-isort pip-requirements live-py-mode hy-mode helm-pydoc cython-mode company-anaconda anaconda-mode pythonic mu4e-maildirs-extension mu4e-alert pdf-tools tablist auto-complete-auctex yasnippet-snippets latex-preview-pane stickyfunc-enhance srefactor disaster company-c-headers cmake-mode clang-format helm-gtags ggtags web-beautify livid-mode skewer-mode simple-httpd json-mode json-snatcher json-reformat js2-refactor multiple-cursors js2-mode js-doc company-tern dash-functional tern coffee-mode insert-shebang fish-mode company-shell web-mode tagedit slim-mode scss-mode sass-mode pug-mode helm-css-scss haml-mode emmet-mode company-web web-completion-data flyspell-popup engine-mode smeargle rainbow-mode rainbow-identifiers orgit magit-gitflow magit-popup helm-gitignore gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link evil-magit color-identifiers-mode selectric-mode emoji-cheat-sheet-plus company-emoji zenburn-theme zen-and-art-theme white-sand-theme underwater-theme ujelly-theme twilight-theme twilight-bright-theme twilight-anti-bright-theme toxi-theme tao-theme tangotango-theme tango-plus-theme tango-2-theme sunny-day-theme sublime-themes subatomic256-theme subatomic-theme spacegray-theme soothe-theme solarized-theme soft-stone-theme soft-morning-theme soft-charcoal-theme smyx-theme seti-theme reverse-theme rebecca-theme railscasts-theme purple-haze-theme professional-theme planet-theme phoenix-dark-pink-theme phoenix-dark-mono-theme organic-green-theme omtose-phellack-theme oldlace-theme occidental-theme obsidian-theme noctilux-theme naquadah-theme mustang-theme monokai-theme monochrome-theme molokai-theme moe-theme minimal-theme material-theme majapahit-theme madhat2r-theme lush-theme light-soap-theme jbeans-theme jazz-theme ir-black-theme inkpot-theme heroku-theme hemisu-theme hc-zenburn-theme gruvbox-theme gruber-darker-theme grandshell-theme gotham-theme gandalf-theme flatui-theme flatland-theme farmhouse-theme exotica-theme espresso-theme dracula-theme django-theme darktooth-theme autothemer darkokai-theme darkmine-theme darkburn-theme dakrone-theme cyberpunk-theme color-theme-sanityinc-tomorrow color-theme-sanityinc-solarized clues-theme cherry-blossom-theme busybee-theme bubbleberry-theme birds-of-paradise-plus-theme badwolf-theme apropospriate-theme anti-zenburn-theme ample-zen-theme ample-theme alect-themes afternoon-theme exwm-edit xterm-color shell-pop multi-term eshell-z eshell-prompt-extras esh-help wgrep smex ivy-hydra flyspell-correct-ivy counsel-projectile counsel swiper ivy exwm xelb yaml-mode ranger visual-fill writeroom-mode visual-fill-column darkroom ob-translate reverse-im company-auctex auctex-latexmk auctex pandoc-mode ox-pandoc ht google-translate flycheck-pos-tip pos-tip flycheck flyspell-correct-helm flyspell-correct auto-dictionary helm-company helm-c-yasnippet fuzzy company-statistics company auto-yasnippet yasnippet ac-ispell auto-complete org-projectile org-category-capture org-present org-pomodoro alert log4e gntp org-mime org-download htmlize gnuplot mmm-mode markdown-toc markdown-mode gh-md ws-butler winum which-key volatile-highlights vi-tilde-fringe uuidgen use-package toc-org spaceline powerline restart-emacs request rainbow-delimiters popwin persp-mode pcre2el paradox spinner org-plus-contrib org-bullets open-junk-file neotree move-text macrostep lorem-ipsum linum-relative link-hint indent-guide hydra lv hungry-delete hl-todo highlight-parentheses highlight-numbers parent-mode highlight-indentation helm-themes helm-swoop helm-projectile projectile pkg-info epl helm-mode-manager helm-make helm-flx helm-descbinds helm-ag golden-ratio flx-ido flx fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist highlight evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state smartparens evil-indent-plus evil-iedit-state iedit evil-exchange evil-escape evil-ediff evil-args evil-anzu anzu evil goto-chg undo-tree eval-sexp-fu elisp-slime-nav dumb-jump f dash s diminish define-word column-enforce-mode clean-aindent-mode bind-map bind-key auto-highlight-symbol auto-compile packed aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line helm avy helm-core popup async)))
 '(paradox-github-token t)
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(perfect-margin-mode nil)
 '(perfect-margin-visible-width 85)
 '(pos-tip-background-color "#414E63")
 '(pos-tip-foreground-color "#BEC8DB")
 '(powerline-color1 "#1E1E1E")
 '(powerline-color2 "#111111")
 '(selectric-mode t)
 '(sml/active-background-color "#34495e")
 '(sml/active-foreground-color "#ecf0f1")
 '(sml/inactive-background-color "#dfe4ea")
 '(sml/inactive-foreground-color "#34495e")
 '(spaceline-show-default-input-method nil)
 '(tabbar-background-color "#353335333533")
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#f36c60")
     (40 . "#ff9800")
     (60 . "#fff59d")
     (80 . "#8bc34a")
     (100 . "#81d4fa")
     (120 . "#4dd0e1")
     (140 . "#b39ddb")
     (160 . "#f36c60")
     (180 . "#ff9800")
     (200 . "#fff59d")
     (220 . "#8bc34a")
     (240 . "#81d4fa")
     (260 . "#4dd0e1")
     (280 . "#b39ddb")
     (300 . "#f36c60")
     (320 . "#ff9800")
     (340 . "#fff59d")
     (360 . "#8bc34a"))))
 '(vc-annotate-very-old-color nil)
 '(visual-fill-column-width 81)
 '(word-wrap t)
 '(writeroom-border-width 45)
 '(writeroom-fullscreen-effect (quote maximized))
 '(writeroom-global-effects
   (quote
    (writeroom-set-fullscreen writeroom-set-alpha writeroom-set-menu-bar-lines writeroom-set-tool-bar-lines writeroom-set-vertical-scroll-bars writeroom-set-bottom-divider-width writeroom-set-internal-border-width)))
 '(writeroom-major-modes (quote (latex-mode markdown-mode org-mode text-mode)))
 '(writeroom-mode-line t)
 '(writeroom-restore-window-config t)
 '(writeroom-width 38)
 '(xterm-color-names
   ["#414E63" "#CC71D1" "#88D6CB" "#C79474" "#76A2D1" "#4A4B6B" "#96A9D6" "#8E95A3"])
 '(xterm-color-names-bright
   ["#555B77" "#E074DB" "#8BE8D8" "#B2DEC1" "#75B5EB" "#9198EB" "#C3C3E8" "#838791"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil))))
 '(fancy-battery-charging ((t (:foreground "forest green"))))
 '(fancy-battery-critical ((t (:foreground "red3"))))
 '(fancy-battery-discharging ((t (:foreground "yellow3")))))
